import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_grafana(host):
    grafana = host.service("grafana-server")
    assert grafana.is_running
    assert grafana.is_enabled


def test_unit_file(host):
    file = host.file("/usr/lib/systemd/system/grafana-server.service")
    assert file.exists
